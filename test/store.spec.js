import Vue from 'vue';
import store from '../src/store';
import { Promise } from 'es6-promise';

// Books
describe('store.books', () => {

  // Fetch books
  it('should NOT be EMPTY', () => {
    
    store.fetchBooks().then( (response) => {
      
      expect(response.data.length).toBeGreaterThan(0);
    });
  });
  
  it('should be EQUAL', () => {
    
    // Fake addCache
    store.addCache([{
      "isbn": "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
      "title": "Henri Potier à l'école des sorciers",
      "price": 35,
      "cover": "http://henri-potier.xebia.fr/hp0.jpg"
    }]);
  
    // Get Fake book cached
    const book = store.getCache('c8fabf68-8374-48fe-a7ea-a00ccd07afff');
    
    // Expect to have the stored book in cache
    expect(book.isbn).toBe('c8fabf68-8374-48fe-a7ea-a00ccd07afff');
  });

});

// Cart
describe('store.cart', () => {

  it('should be EMPTY', () => {
    
    // Empty cart at start
    const cart = store.getCart();
    expect(cart.length).toBe(0);
  });

  it('should have ONE book', () => {
    
    // Add book in cart
    store.addCartBook('fake-isbn');
    
    // Expect to have one book in cart
    const cart = store.getCart();
    expect(cart.length).toBe(1);
  });

  it('should be EMPTY', () => {
    
    // Clear cart
    store.clearCart();
    
    // Add book in cart
    store.addCartBook('fake-isbn');
    
    // Remove this book
    store.removeCartBook(store.getCart()[0]);
    
    // Excpect to be empty
    const cart = store.getCart();
    expect(cart.length).toBe(0);
  });

});

// Offers
describe('store.offers', () => {
  
  // Isbns to send WS
  const isbns = [
    'c8fabf68-8374-48fe-a7ea-a00ccd07afff', // HP1
    'a460afed-e5e7-4e39-a39d-c885c05db861'  // HP22
  ];
  
  // Offers from WS
  const offers = [
    {
      "type": "percentage",
      "value": 4
    },
    {
      "type": "minus",
      "value": 15
    },
    {
      "type": "slice",
      "sliceValue": 100,
      "value": 12
    }
  ];
  
  // Total Price of the offer
  const totalPrice = 65;
  
  it('should NOT be Empty', () => {
    
    store.fetchOffersByIsbns(isbns).then( (response) => {
      
      expect(response.data.length).toBeGreaterThan(0);
    });
  });
  
  it('should Be 62.4', () => {

    expect(store.checkPercentageOffer(offers, totalPrice)).toBe(62.4);
  });
  
  it('should Be 50', () => {

    expect(store.checkMinusOffer(offers, totalPrice)).toBe(50);
  });
  
  it('should Be 65', () => {

    expect(store.checkSliceOffer(offers, totalPrice)).toBe(65);
  });
  
  it('should Be EQUAL Minus Offer', () => {

    const bestOffer = store.getBestOffer(offers, totalPrice);

    expect(bestOffer).toEqual({
      'name': 'Offre par réduction direct',
      'value': 50
    });
  });
  

});