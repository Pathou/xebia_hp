

# Test Front Xebia



1 Introduction
=====================

Le but du test est de créer un site e-commerce comprenant deux écrans, un permettant d'afficher les livres et de les mettre dans le panier. Le second récapitule le panier et applique la meilleure offre retournée par les WS de Xebia

J'ai ajouté trois écrans, un écran de contact, un de mentions légales et un dernier permettant d'afficher les détails d'un livre avec des informations acquises par un webservice créé spécialement pour le test.

2 Installation
=====================
a Front end
--------------------
Cloner le dépôt

### [https://bitbucket.org/Pathou/xebia\_hp](https://bitbucket.org/Pathou/xebia_hp)

Créer le fichier de configuration

Copier le fichier /config/config.dist.js vers /config/config.js et ajouter les URLs à appeler

Lancer en mode Dev

A la racine du projet :

> npm run dev

Construire le projet en mode Production

A la racine du projet :

> npm run build

b Back end
--------------------
Cloner le dépôt

[https://bitbucket.org/Pathou/xebia\_hp\_back](https://bitbucket.org/Pathou/xebia_hp_back)

Lancer le serveur web

A la racine du projet :

> node main.js

_Le module Forever pourra être utilisé dans le cas d'une vraie mise en prod_
