import { Promise } from 'es6-promise'
import Vue from 'vue';
import Router from 'vue-router';
import Resource from 'vue-resource';
import Config from '../../config/config.js'; // Application config

Vue.use(Router);
Vue.use(Resource);

// Books Cache var
const booksCache = {};
// BooksInfos Cache var
const booksCacheInfos = {};
// Books in cart
var cartBooks = [];
// Router
const router = new Router();
// Store init
const store = {};

export default store

/**
 * Fetch books from backend
 *
 * @return {Promise}
 */
store.fetchBooks = () => {

  return new Promise((resolve, reject) => {

    // If already in cache
    if(Object.keys(booksCache).length) {
      resolve(booksCache);
    }
    else {
      Vue.http({url: Config.urlBooks, method: 'GET'}).then( (response) => {

        // Add book in cache
        store.addCache(response.data);

        resolve(response.data);
      }, (response) => {

        reject(response);
      });
    }
  });
}

/**
 * Add an book to the cache
 *
 * @param {Array} books
 * @return void
 */
store.addCache = books => {
	books.forEach(book => {
		booksCache[book.isbn] = book;
	});
}

/**
 * Get an book from the cache
 *
 * @param {String}
 * @return {Object} book
 */
store.getCache = isbn => {
  const book = booksCache[isbn];
    
  if(book) return book;     // if book in cache
  else router.go('/list');  // else relocate to list
}

/**
 * Fetch infos from backend ext
 *
 * @param {String}
 * @return {Promise}
 */
store.fetchBooksInfos = isbn => {

  return new Promise( (resolve, reject) => {
    
    // If already in cache
    if(booksCacheInfos[isbn] != null) {
      resolve(booksCacheInfos[isbn]);
    }
    else {
      // else, get Infos by WS
      Vue.http({url: Config.urlIsbn + isbn, method: 'GET'}).then( (response) => {

        const infos = response.data;
        
        store.addCacheInfos(infos.isbn, infos);
        resolve(infos);

      }, (response) => {

        reject(response)
      });
    }
  });
}

/**
 * Add an book infos to the cache
 *
 * @param {Array} books
 * @return void
 */
store.addCacheInfos = (isbn, infos) => {
  booksCacheInfos[isbn] = infos;
}


/**
 * Add an book in the cart
 *
 * @param {Object} string
 * @return void
 */
store.addCartBook = isbn => {
  cartBooks.push(store.getCache(isbn));
}

/**
 * Clear all books in the cart
 *
 * @param void
 * @return void
 */
store.clearCart = () => {
  cartBooks = [];
}

/**
 * Remove one books in the cart
 *
 * @param {Object} book
 * @return void
 */
store.removeCartBook = (book) => {
  cartBooks.$remove(book);
}

/**
 * Get all books in the cart
 *
 * @param void
 * @return {Array} book
 */
store.getCart = () => {
  return cartBooks;
}

/**
 * Fetch an book data with given id.
 *
 * @param {Array<String>} isbns
 * @return {Promise}
 */
store.fetchOffersByIsbns = isbns => {

  return new Promise( (resolve, reject) => {
    Vue.http({url: Config.urlOffers.replace('{0}', isbns.join(',')), method: 'GET'}).then(response => {
      
      resolve(response.data);
    }, (response) => {
      
      reject({});
    });
  });
}

/**
 * Check Percentage offer.
 *
 * @param {Array<Object>} offers
 * @param {float} basePrice
 * @return {float}
 */
store.checkPercentageOffer = (offers, basePrice) => {
  
  const offerFilter = offers.filter(o => o.type === 'percentage');
  if(offerFilter.length === 0) return null;
  const offer = offerFilter[0];
  
  return basePrice * ( (100 - offer.value) / 100);
}

/**
 * Check Minus offer.
 *
 * @param {Array<Object>} offers
 * @param {float} basePrice
 * @return {float}
 */
store.checkMinusOffer = (offers, basePrice) => {
  
  const offerFilter = offers.filter(o => o.type === 'minus');
  if(offerFilter.length === 0) return null;
  const offer = offerFilter[0];
  const diff = basePrice - offer.value;
  
  return diff > 0 ? diff : 0;
}

/**
 * Check Slice offer.
 *
 * @param {Array<Object>} offers
 * @param {float} basePrice
 * @return {float}
 */
store.checkSliceOffer = (offers, basePrice) => {
  
  const offerFilter = offers.filter(o => o.type === 'slice');
  if(offerFilter.length === 0) return null;
  const offer = offerFilter[0];
  
  // On récupère le coefficient de rabais
  const coeff = Math.floor(basePrice / offer.sliceValue);
  
  return basePrice - (offer.value * coeff);
}

/**
 * Get the best offer.
 *
 * @param {Array<Object>} offers
 * @param {float} totalPrices
 * @return {Object}
 */
store.getBestOffer = (offers, totalPrice) => {
  const percentageOffer = store.checkPercentageOffer(offers, totalPrice);
  const minusOffer = store.checkMinusOffer(offers, totalPrice);
  const sliceOffer = store.checkSliceOffer(offers, totalPrice);
  
  if(sliceOffer !== null && minusOffer !== null) {
    if(sliceOffer < minusOffer && sliceOffer < percentageOffer) {
      return {
        'name': 'Offre par Tranche',
        'value': sliceOffer
      };
    }
    else if(minusOffer < sliceOffer && minusOffer < percentageOffer) {
      return {
        'name': 'Offre par réduction direct',
        'value': minusOffer
      };
    }
  }

  return {
    'name': 'Offre par Pourcentage',
    'value': percentageOffer
  };
}