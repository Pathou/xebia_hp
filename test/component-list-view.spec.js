import Vue from 'vue';
import ListView from '../src/components/ListView.vue';

describe('ListView.vue', () => {

  // Test books EMPTY
  it('should be Empty', () => {
    expect(ListView.data().books.length).toBe(0)
  });
})