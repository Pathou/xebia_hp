import Vue from 'vue';
import Router from 'vue-router';
import Resource from 'vue-resource';
import { toPrice, toStars } from './filters';
import App from './components/App.vue';
import ListView from './components/ListView.vue';
import BookView from './components/BookView.vue';
import ContactView from './components/ContactView.vue';
import CartReviewView from './components/CartReviewView.vue';
import GeneralConditionsView from './components/GeneralConditionsView.vue';

// install router and ressource
Vue.use(Router);
Vue.use(Resource);

// register filters globally
Vue.filter('toPrice', toPrice);
Vue.filter('toStars', toStars);

// routing
var router = new Router();

router.map({
  '/list': {
    component: ListView
  },
  '/list/:isbn': {
    component: BookView
  },
  '/cart-review': {
    component: CartReviewView
  },
  '/contact': {
    component: ContactView
  },
  '/general-conditions': {
    component: GeneralConditionsView
  }
});

// Scroll top every route change
router.beforeEach(() => {
  window.scrollTo(0, 0);
});

// 404 = /list
router.redirect({
  '*': '/list'
});

// Start the App
router.start(App, '#app');
