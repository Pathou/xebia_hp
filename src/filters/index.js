// Filters

// Transform number in printable price
export function toPrice (price, currency = '€') {

  return parseFloat(price).toFixed(2) + ' ' + currency;
}

// Transform number to stars
export function toStars (number) {

  const nbStars = Math.floor(number);
  
  var stars = [];
  
  for(let i=0; i < 5; i++) {
    if(i < nbStars) stars.push('<i class="glyphicon glyphicon-star"></i>');
    else stars.push('<i class="glyphicon glyphicon-star-empty"></i>');
  }
  return stars.join(' ');
}